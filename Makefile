GC?=1
VALUES?=1
MOPSA_OPTIONS?=
OUTPUT_DIR?=.

ifneq (,$(findstring json,$(MOPSA_OPTIONS)))
	MOPSA_OUT_EXT=json
else
	MOPSA_OUT_EXT=txt
endif


ifeq ($(VALUES), 1)
	MOPSAC=mopsa-python -silent
else
	MOPSAC=mopsa-python-types -silent
endif

ifeq ($(GC),1)
	MOPSA=$(MOPSAC) -gc $(MOPSA_OPTIONS)
else
	MOPSA=$(MOPSAC) $(MOPSA_OPTIONS)
endif

all: choose processInput

choose:
	mkdir -p ${OUTPUT_DIR}
	cd src && $(MOPSA) $@.py 1>../${OUTPUT_DIR}/$@_${GC}_${VALUES}.${MOPSA_OUT_EXT} 2>../${OUTPUT_DIR}/$@_${GC}_${VALUES}.stderr

processInput:
	mkdir -p ${OUTPUT_DIR}
	cd src && $(MOPSA) $@.py 1>../${OUTPUT_DIR}/$@_${GC}_${VALUES}.${MOPSA_OUT_EXT} 2>../${OUTPUT_DIR}/$@_${GC}_${VALUES}.stderr
