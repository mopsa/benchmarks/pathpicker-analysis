This repository contains extracts of Facebook's [PathPicker utility](https://github.com/facebook/PathPicker), slightly adapted for analyzing them using Mopsa.
The source code is based on commit a2fdbb60.

The continuous integration files tests 4 configuration of the analysis:
- type or value analysis
- with or without the abstract garbage collection enabled

A Makefile is also provided.
It assumes `mopsa` is built and its `bin` directory is in the PATH.
You can pass `GC=0` and `VALUES=0` to respectively disable the abstract garbage collection or the value analysis (in favor of only the type analysis).
The make targets `choose`, `processInput` and `all` to run both tests.
Mopsa will then list all the exceptions it thinks Python may raise.
For each exception, it provides the exception name and a message close to the one provided by CPython.
In addition, it shows where the exception would be raised and what the call stack would be at that point.


Stubbed files:
- sys.py
- os.pyi (to avoid abc classes)
- re.pyi (the re.py import sre_compile import _sre being a c file)
- subprocess.pyi (uses _posixsubprocess being some c file)
- json.pyi
- curses.pyi (uses some c)
- collections.pyi
- time.pyi (c file timemodule.c)
- posixpath.pyi
- stat.pyi (py file uses _stat.c)
- errno.pyi (c file errnomodule)
- zlib.pyi (c file zlibmodule)
- bz2.pyi (py file uses _bz2modules)
- lzma.pyi (py file uses _lzmamodule)
- locale.pyi (uses _locale but there is a python emulation we could use?)
- signal.pyi (uses _signalmodule)
- enum.pyi (metaclasses)
- argparse.pyi (argparse.py uses a lot of super(), and some terrible things _get_handler: l1561)
